import 'picturefill'
import 'intersection-observer'
import 'what-input'
import _ from 'lodash'
import Collapse from '../modules/_Collapse'
import Scroller from '../modules/_scroller'

// Collapse

window.addEventListener('load', () => {
  _.forEach(document.querySelectorAll('[data-collapse-toggler]'), el => {
    const collapse = new Collapse(el, {
      hashNavigation: true
    })
    collapse.init()
  })
})

// scroller
const scroller = new Scroller('.js-scroller')
scroller.init()
{
  const option = {
    rootMargin: '0px',
    threshold: [0]
  }

  class ToggleContactBtn {
    constructor() {
      this.trigger = document.getElementById('sec-contact')
      this.target = document.getElementById('js-fadeout-target')
    }

    init() {
      const observer = new IntersectionObserver(
        this.isTargetCross.bind(this),
        option
      )
      observer.observe(this.trigger)
    }

    isTargetCross(entries, observer) {
      entries.forEach(entry => {
        // 交差している場合はtrue
        if (entry.isIntersecting) {
          this.target.style.opacity = 0
          this.target.style.visibility = 'hidden'
        } else {
          this.target.style.opacity = 1
          this.target.style.visibility = 'visible'
        }
      })
    }
  }

  const TOGGLE_CONTACT_BTN = new ToggleContactBtn()
  TOGGLE_CONTACT_BTN.init()
}

{
  class ModalToggle {
    constructor() {
      this.targetParent = document.querySelector('.footer-modal')
      this.targets = document.querySelectorAll('[js-modal-target]')
      this.triggers = document.querySelectorAll('[js-modal-btn]')
      this.closeTrigger = document.querySelector('[js-modal-close]')
    }

    init() {
      this.triggerClick()
      this.modalClose()
    }

    triggerClick() {
      this.triggers.forEach(trigger => {
        trigger.addEventListener('click', () => {
          this.modalOpen(trigger.getAttribute('js-modal-btn'))
        })
      })
    }

    modalOpen(value) {
      this.targetParent.classList.add('is-active')
      const TARGET = document.getElementById(value)
      TARGET.classList.add('is-active')
    }

    modalClose() {
      this.closeTrigger.addEventListener('click', () => {
        this.targetParent.classList.remove('is-active')
        const CLOSE_TARGET = document.querySelector('.modal.is-active')
        CLOSE_TARGET.classList.remove('is-active')
      })
    }
  }

  const MODAL_TOGGLE = new ModalToggle()
  MODAL_TOGGLE.init()
}
